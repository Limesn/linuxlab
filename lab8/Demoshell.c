#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include "logout_exit.h"
#include "parse.h"
#define MAXLINE 4096

int main(void)
{
    pid_t pid;
    char *rt;
    char buf[MAXLINE];
    int status;
    char *args[64];
    int argnum = 0;
    int flag = 0;

    while (1)
    {
        printf("%%# "); // 打印提示符
        rt = fgets(buf, MAXLINE, stdin);
        if (rt == NULL)
        {
            printf("fgets error\n");
            exit(1);
        }
        if (!strcmp(buf, "\n")) // 处理仅输入回车的情况
        {
            printf("%%# "); // 打印提示符
            continue;      // 结束循环
        }
        if (buf[strlen(buf) - 1] == '\n')
            buf[strlen(buf) - 1] = 0; // 使用NULL替换新行

        argnum = parse(buf, args); // 分析用户输入的命令

        logout_exit(args, &pid, buf, &flag, &status);
        if (flag)
        {
            continue;
            flag = 0;
        }
    }
    exit(0);
}
