#include <stdlib.h>
/// @brief 处理用户退出
/// @param args 传入用户输入的参数
/// @param pid 
/// @param buf 
/// @param flag 循环标志位
/// @param status 
void logout_exit(char *args[], pid_t *pid, char buf[], int *flag, int *status);