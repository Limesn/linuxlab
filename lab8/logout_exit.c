#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

void logout_exit(char *args[], pid_t *pid, char buf[], int *flag, int *status)
{

    if ((strcmp(args[0], "logout") == 0) || (strcmp(args[0], "exit") == 0)) // exit shell
        exit(0);
    else // execute other command
    {
        if ((*pid = fork()) < 0) // fork
        {
            printf("fork error,please reput command\n");
            *flag = 1;//置标志位为1，以便在主函数中结束循环
        }
        else if (*pid == 0)
        { // child
            execvp(*args, args);
            printf("couldn't execute: %s\n", buf);
            exit(127);
        }
        // parent
        if ((*pid = waitpid(*pid, status, 0)) < 0)
            printf("waitpid error\n");
    }
}
