int parse(char *buf, char **args)
{
    int num = 0;
    while (*buf != '\0')
    {
        while ((*buf == ' ') || (*buf == '\t' || (*buf == '\n')))
            *buf++ = '\0'; //该循环是定位到命令中每个字符串的第一个非空的字符
        *args++ = buf;     //将找到的非空字符串 依次赋值给args［i］。
        ++num;

        while ((*buf != '\0') && (*buf != ' ') && (*buf != '\t') && (*buf != '\n')) //正常的字母就往后移动，直至定位到非空字符后面的第一个空格。
            buf++;
    }
    *args = '\0';
    return num;
}